# frozen_string_literal: true

Rails.application.routes.draw do
  resource :sessions, only: [:create]

  resources :users, only: %i[create update] do
    scope module: :users do
      resources :messaged_users, only: [:index]
      resource :mentar, only: %i[show update]
    end

    collection do
      get :current_user, to: "users#show"
      scope module: :users do
        get :consultations, to: "consultations#index"
        resources :with_id, only: [:show]
      end
    end
  end

  resources :mentars, only: %i[index show] do
    collection do
      get :index, to: "mentars#index"
      get :detail, to: "mentars#detail"
      post :update_c, to: "mentars#update_c"
      scope module: :mentars do
        get :sort, to: "sorts#index"
        get :search, to: "search#index"
      end
    end
  end

  resource :direct_message_comment, only: %i[show create]

  resources :notifications, only: %i[index create destroy] do
    collection do
      scope module: :notifications do
        delete :all, to: "all#destroy"
      end
    end
  end

  resources :consultations, only: %i[index create show] do
    scope module: :consultations do
      resource :status, only: [:update]
    end
    collection do
      get :user_consultations, to: "consultations#user_consultations"
      scope module: :consultations do
        get :sort, to: "sorts#index"
        get :search, to: "search#index"
      end
    end
  end

  resources :comments, only: %i[index create]

  namespace :admin do
    get '/', to: "home#index"
    resources :users do
      resource :user_logs, only: [:show]

      scope module: :users do
        resource :freeze, only: [:create, :destroy]
      end
    end

  end
end
