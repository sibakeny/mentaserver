# ルート
crumb :root do
  link "管理トップ", admin_path
end

# ユーザー詳細
crumb :admin_user_path do
  link "ユーザー詳細", admin_user_path
end

crumb :admin_users do
  link "ユーザー一覧", admin_users_path
end

crumb :admin_users_show do
  link "ユーザー一覧", admin_users_path
  link "ユーザー詳細", admin_user_path
end

crumb :user_logs do |user| 
  link "ユーザー一覧", admin_users_path
  link "ユーザー詳細", admin_user_path(user)
  link "ログイン履歴"
end