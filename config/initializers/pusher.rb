# frozen_string_literal: true

require "pusher"

MSPusher = Pusher::Client.new(
  app_id: ENV["PUSHER_APP_API"],
  key: ENV["PUSHER_KEY"],
  secret: ENV["PUSHER_SECRET"],
  cluster: ENV["PUSHER_CLUSTER"],
  encrypted: ENV["PUSHER_ENCRYPTED"],
)

Pusher.logger = Rails.logger
