# アプリ名

MentaServer

## 詳細

モバイルアプリ MentaSupport の API 機能と、アプリの管理を行う

## デモ

## 開発方法

##### 全般

- bullet にて N+1 のチェックを行う
- rubocop にてコードのチェック

##### Model

- validateion のテスト
- パブリックメソッドのテスト

##### Controller

- api の input に対する output のテスト
- view に関してはテストしない
- controller には index,show,new,create,edit,update,destroy 以外のメソッドは書かない

##### セキュリティ

- brakeman でセキュリティのチェック

## CI

GitLab CI を利用してコミット時に rspec と rubocop をチェック

## 作者

[sibakeny](https://twitter.com/SibakenY)
