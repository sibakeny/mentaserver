# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) {|repo| "https://github.com/#{repo}.git" }

ruby "2.6.5"

gem "jbuilder", "~> 2.7"
gem "puma", "~> 4.1"
gem "rails", "~> 6.0.2", ">= 6.0.2.1"
gem "sass-rails", ">= 6"
gem "sqlite3", "~> 1.4"
gem "turbolinks", "~> 5"
gem "webpacker", "~> 4.0"
# gem 'bcrypt', '~> 3.1.7'
gem "pusher"

gem "fast_jsonapi"

gem "dotenv-rails"

gem "image_processing", "~> 1.2"
gem "mini_magick"

gem "bootsnap", ">= 1.4.2", require: false
gem 'font-awesome-sass'

gem "gretel"

gem "chartkick"
gem 'groupdate'

group :development, :test do
  gem "bullet"
  gem "byebug", platforms: %i[mri mingw x64_mingw]
end

group :development do
  gem "brakeman", require: false
  gem "rubocop"
  gem "web-console", ">= 3.3.0"
end

group :test do
  gem "capybara", ">= 2.15"
  gem "database_cleaner"
  gem "factory_bot_rails"
  gem "rspec-rails", "~> 3.6"
  gem "selenium-webdriver"
  gem "simplecov"
  gem "webdrivers"
end

gem "tzinfo-data", platforms: %i[mingw mswin x64_mingw jruby]
