FactoryBot.define do
  factory :consultation do
    category { "仕事" }
    sex { 1 }
    title { "title" }
    description { "description" }
  end
end
