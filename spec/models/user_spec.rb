require "rails_helper"

RSpec.describe User, type: :model do
  describe "作成" do
    context "パラメータがすべてある場合" do
      it "ユーザー登録が出来ること" do
        user = User.new(name: "user", email: "email@example.com", password: "password", description: "description")
        expect(user).to be_valid
      end

      it "作成されたユーザーにtokenがあること" do
        user = User.create(name: "user", email: "email@example.com", password: "password", description: "description")
        expect(user.token.blank?).to be_falsey
      end
    end

    context "emailがない場合" do
      it "ユーザーの作成に失敗すること" do
        user = User.new(name: "user", password: "password", description: "description")
        expect(user).to_not be_valid
      end
    end

    context "nameがない場合" do
      it "ユーザーの作成に失敗すること" do
        user = User.new(email: "email@example.com", password: "password", description: "description")
        expect(user).to_not be_valid
      end
    end
  end
end
