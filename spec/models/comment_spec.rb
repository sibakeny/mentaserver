require "rails_helper"

RSpec.describe Comment, type: :model do
  describe "作成" do
    let(:user) { create(:user) }
    let!(:consultation) { create(:consultation, user_id: user.id) }
    let(:sender) { create(:user, name: "sender", email: "sender@example.com") }
    let(:reciever) { create(:user, name: "reciever", email: "reciever@example.com") }
    context "パラメータがすべてある場合" do
      it "作成が出来ること" do
        comment = consultation.comments.new(sender_id: sender.id, reciever_id: reciever.id, body: "test")
        expect(comment).to be_valid
      end
    end

    context "sender_idがない場合" do
      it "作成出来ない" do
        comment = consultation.comments.new(reciever_id: reciever.id, body: "test")
        expect(comment).to_not be_valid
      end
    end

    context "reciever_idがない場合" do
      it "作成出来ること" do
        comment = consultation.comments.new(sender_id: sender.id, body: "test")
        expect(comment).to be_valid
      end
    end
  end
end
