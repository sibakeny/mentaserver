require "rails_helper"

RSpec.describe Mentar, type: :model do
  describe "作成" do
    let(:user) { create(:user) }
    context "パラメータがすべてある場合" do
      it "作成出来ること" do
        mentar = Mentar.new(description: "description", category: "category", sex: 1, user_id: user.id)
        expect(mentar).to be_valid
      end
    end

    context "user_idがない場合" do
      it "作成出来ないこと" do
        mentar = Mentar.new(description: "description", category: "category", sex: 1)
        expect(mentar).to_not be_valid
      end
    end

    it "ユーザーが複数のmentarを作成出来ないこと"
  end
end
