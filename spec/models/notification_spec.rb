require "rails_helper"

RSpec.describe Notification, type: :model do
  describe "作成" do
    let(:user) { create(:user) }
    let(:sender) { create(:user, name: "sender", email: "sender@example.com") }
    let!(:consultation) { create(:consultation, user_id: user.id) }
    context "パラメータがすべてある場合" do
      it "通知の作成が出来ること" do
        notification = consultation.notifications.new(user_id: user.id, sender_id: sender.id, category: 1)
        expect(notification).to be_valid
      end
    end

    context "user_idがない場合" do
      it "通知の作成が出来ないこと" do
        notification = consultation.notifications.new(sender_id: sender.id, category: 1)
        expect(notification).to_not be_valid
      end
    end

    context "sender_idがない場合" do
      it "通知の作成が出来ないこと" do
        notification = consultation.notifications.new(user_id: user.id, category: 1)
        expect(notification).to_not be_valid
      end
    end

    context "categoryがない場合" do
      it "通知の作成が出来ること" do
        notification = consultation.notifications.new(user_id: user.id, sender_id: sender.id)
        expect(notification).to be_valid
      end
    end
  end
end
