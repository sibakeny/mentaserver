# frozen_string_literal: true

class MentarSerializer
  include FastJsonapi::ObjectSerializer
  include Rails.application.routes.url_helpers

  attributes :id, :description, :category, :sex
  belongs_to :user
  has_many :comments, as: :target

  attribute :user_image_url do |object|
    Rails.application.routes.default_url_options[:host] = "localhost:3000"
    if object.user.image.attached?
      Rails.application.routes.url_helpers.rails_blob_url(object.user.image)
    end
  end

  attribute :user_name do |object|
    object.user.name
  end
end
