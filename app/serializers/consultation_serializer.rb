# frozen_string_literal: true

class ConsultationSerializer
  include FastJsonapi::ObjectSerializer
  include Rails.application.routes.url_helpers

  attributes :id, :title, :description, :category, :sex, :status
  belongs_to :user

  attribute :image_url do |object|
    Rails.application.routes.default_url_options[:host] = "localhost:3000"
    if object.user.image.attached?
      Rails.application.routes.url_helpers.rails_blob_url(object.user.image)
    end
  end

  attribute :user_name do |object|
    object.user.name
  end
end
