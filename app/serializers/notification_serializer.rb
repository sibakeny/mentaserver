# frozen_string_literal: true

class NotificationSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :category, :opponent_type, :opponent_id
  belongs_to :sender, class_name: "User", foreign_key: "sender_id"

  attribute :sender_name do |object|
    object.sender.name
  end
end
