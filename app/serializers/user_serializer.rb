# frozen_string_literal: true

class UserSerializer
  include FastJsonapi::ObjectSerializer
  include Rails.application.routes.url_helpers

  attributes :id, :name, :email, :description, :token

  has_one :mentar

  attribute :image_url do |object|
    Rails.application.routes.default_url_options[:host] = "localhost:3000"
    if object.image.attached?
      Rails.application.routes.url_helpers.rails_blob_url(object.image)
    end
  end

  attribute :latest_comment do |object, params|
    if params && params[:currentUserId]
      Comment.where(
        reciever_id: object.id,
        sender_id: params[:currentUserId],
        target_type: nil,
      ).or(
        Comment.where(
          sender_id: object.id,
          reciever_id: params[:currentUserId],
          target_type: nil,
        ),
      ).order(created_at: :desc).first&.body
    end
  end

  attribute :notification_count do |object, params|
    if params && params[:currentUserId]
      Notification.where(user_id: params[:currentUserId], sender_id: object.id, category: "directMessage").count
    end
  end
end
