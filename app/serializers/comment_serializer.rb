# frozen_string_literal: true

class CommentSerializer
  include FastJsonapi::ObjectSerializer
  include Rails.application.routes.url_helpers

  attributes :id, :body, :created_at
  belongs_to :target
  belongs_to :sender, class_name: "User", foreign_key: "sender_id", optional: true
  belongs_to :reciever, class_name: "User", foreign_key: "reciever_id", optional: true

  attribute :image_url do |object|
    Rails.application.routes.default_url_options[:host] = "localhost:3000"
    if object&.target&.user&.image&.attached?
      Rails.application.routes.url_helpers.rails_blob_url(object&.target&.user&.image)
    elsif object&.sender&.image&.attached?
      Rails.application.routes.url_helpers.rails_blob_url(object&.sender&.image)
    end
  end

  attributes :user_name do |object|
    object.sender&.name
  end
end
