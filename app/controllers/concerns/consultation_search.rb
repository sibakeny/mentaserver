# frozen_string_literal: true

module ConsultationSearch
  extend ActiveSupport::Concern

  included do
    def search
      @consultations = Consultation.includes({ user: [image_attachment: :blob] }).all.where(status: "open").order(created_at: :desc)

      @consultations = @consultations.filter(params)
    end
  end
end
