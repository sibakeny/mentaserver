# frozen_string_literal: true

module MentarSearch
  extend ActiveSupport::Concern

  included do
    def search
      @mentars = Mentar.includes({ user: [image_attachment: :blob] }).all

      @mentars = @mentars.filter(params)
    end
  end
end
