# frozen_string_literal: true

class NotificationsController < ApplicationController
  def index
    user = User.find_by(token: params[:token])
    notifications = Notification.includes(:sender).where(user_id: user.id)

    json_string = NotificationSerializer.new(notifications).serialized_json

    render json: json_string
  end

  def create
    user = User.find_by(token: params[:token])
    Notification.create(user_id: user.id, sender_id: params[:sender_id], category: params[:category])
  end

  def destroy
    notification = Notification.find(params[:id])
    notification.destroy
  end
end
