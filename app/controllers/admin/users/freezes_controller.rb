class Admin::Users::FreezesController < ApplicationController
    before_action :set_user

    def create
        @user.freeze_account
    end

    def destroy
        @user.restart_account
    end

    private

    def set_user
        @user = User.find(params[:user_id])
    end
end
