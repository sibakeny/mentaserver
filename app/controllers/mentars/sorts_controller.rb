# frozen_string_literal: true

class Mentars::SortsController < ApplicationController
  include MentarSearch

  def index
    search

    if params[:column] == "comments"
      @mentars = @mentars.joins(:comments).select('"mentars".*', 'count("comments".id) AS cocs').group("mentars.id").order(cocs: :desc)
    elsif params[:column].present?
      @mentars = @mentars.flex_sort(params[:column], params[:order])
    end

    json_string = MentarSerializer.new(@mentars).serialized_json
    render json: json_string
  end
end
