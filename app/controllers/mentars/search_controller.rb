# frozen_string_literal: true

class Mentars::SearchController < ApplicationController
  include MentarSearch

  def index
    search

    json_string = MentarSerializer.new(@mentars).serialized_json
    render json: json_string
  end
end
