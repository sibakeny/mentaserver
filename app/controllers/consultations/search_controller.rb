# frozen_string_literal: true

class Consultations::SearchController < ApplicationController
  include ConsultationSearch

  def index
    search

    json_string = ConsultationSerializer.new(@consultations).serialized_json
    render json: json_string
  end
end
