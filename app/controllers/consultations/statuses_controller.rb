# frozen_string_literal: true

class Consultations::StatusesController < ApplicationController
  def update
    consultation = Consultation.find(params[:consultation_id])
    consultation.update(status: params[:status])
  end
end
