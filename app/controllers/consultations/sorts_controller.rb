# frozen_string_literal: true

class Consultations::SortsController < ApplicationController
  include ConsultationSearch

  def index
    search

    if params[:column] == "comments"
      @consultations = @consultations.joins(:comments).select('"consultations".*', 'count("comments".id) AS cocs').group("consultations.id").order(cocs: :desc)
    elsif params[:column].present?
      @consultations = @consultations.flex_sort(params[:column], params[:order])
    end

    json_string = ConsultationSerializer.new(@consultations).serialized_json
    render json: json_string
  end
end
