# frozen_string_literal: true

class CommentsController < ApplicationController
  def index
    comments = Comment.includes({ target: :user }, :sender).find_by_params(
      target_id: params[:target_id],
      target_type: params[:target_type],
      sender_id: params[:sender_id],
      reciever_id: params[:reciever_id],
    ).order(created_at: :desc).limit(20).offset(params[:page].to_i * 20)

    json_string = CommentSerializer.new(comments).serialized_json
    render json: json_string
  end

  def create
    @comment = Comment.create!(comment_params)

    create_notification
  end

  private

    def comment_params
      params.require(:comment).permit(
        :sender_id,
        :reciever_id,
        :target_id,
        :target_type,
        :body,
      )
    end

    # 通知の作成
    def create_notification
      user = User.find_by(token: params[:token])
      return if @comment.target.user_id == user.id

      Notification.create!(
        user_id: params[:reciever_id],
        sender_id: params[:sender_id],
        category: params[:target_type],
        opponent_id: @comment.target_id,
        opponent_type: @comment.target_type,
      )
    end
end
