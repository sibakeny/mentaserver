# frozen_string_literal: true

class DirectMessageCommentsController < ApplicationController
  def create
    @comment = Comment.create(
      sender_id: params[:sender_id],
      reciever_id: params[:reciever_id],
      body: params[:body],
    )

    MSPusher.trigger(room_id, "my-event",
                     body: @comment.body,
                     id: @comment.id,
                     createdAt: @comment.created_at,
                     sender: {
                       id: @comment.sender.id,
                     })

    create_notification
  end

  def show
    direct_messages = Comment.includes(:target, :sender).where(
      sender_id: params[:sender_id],
      reciever_id: params[:reciever_id],
      target_type: nil,
    ).or(Comment.includes(:target, :sender).where(
           reciever_id: params[:sender_id],
           sender_id: params[:reciever_id],
           target_type: nil,
         )).order(created_at: :desc).limit(50).offset(params[:page].to_i * 50)

    json_string = CommentSerializer.new(direct_messages).serialized_json
    render json: json_string
  end

  private

    # 通知の作成
    def create_notification
      Notification.find_or_create_by!(
        user_id: params[:reciever_id],
        sender_id: params[:sender_id],
        category: "directMessage",
        opponent_id: @comment.target_id,
        opponent_type: @comment.target_type,
      )
    end

    def room_id
      if params[:reciever_id].to_i > params[:sender_id].to_i
        params[:sender_id].to_s + "_" + params[:reciever_id].to_s
      else
        params[:reciever_id].to_s + "_" + params[:sender_id].to_s
      end
    end
end
