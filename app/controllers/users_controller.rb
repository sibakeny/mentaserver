# frozen_string_literal: true

class UsersController < ApplicationController
  require "pusher"

  def create
    User.create(email: params[:email], password: params[:password], name: params[:name], image: params[:image])
  end

  def show
    user = User.find_by(token: params[:token])

    json_string = UserSerializer.new(user).serialized_json
    render json: json_string
  end

  def update
    user = User.find_by(token: params[:id])
    user.update!(user_params)
    render status: 200, json: { status: 200, message: "Success" }
  end

  private

    def user_params
      params.require(:user).permit(:name, :description, :image)
    end
end
