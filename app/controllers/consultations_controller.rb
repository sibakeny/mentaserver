# frozen_string_literal: true

class ConsultationsController < ApplicationController
  def index
    consultations = Consultation.includes({ user: [image_attachment: :blob] }).where(status: "open").all.order(created_at: :desc)

    json_string = ConsultationSerializer.new(consultations).serialized_json
    render json: json_string
  end

  def create
    consultation = Consultation.create(
      title: params[:title],
      sex: params[:sex],
      category: params[:category],
      description: params[:description],
      user_id: params[:user_id],
    )

    json_string = ConsultationSerializer.new(consultation).serialized_json
    render json: json_string
  end

  def show
    consultation = Consultation.includes(:user).find(params[:id])

    json_string = ConsultationSerializer.new(consultation).serialized_json
    render json: json_string
  end
end
