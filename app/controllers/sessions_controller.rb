# frozen_string_literal: true

class SessionsController < ApplicationController
  def create
    user = User.find_by(email: params[:params][:email], password: params[:params][:password])

    user.create_login_log

    json_string = UserSerializer.new(user).serialized_json
    render json: json_string
  end

  private
  
end
