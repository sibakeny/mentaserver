# frozen_string_literal: true

class MentarsController < ApplicationController
  protect_from_forgery with: :null_session
  skip_before_action :verify_authenticity_token

  def index
    mentars = Mentar.includes({ user: [image_attachment: :blob] }).order(created_at: :desc).all

    json_string = MentarSerializer.new(mentars).serialized_json
    render json: json_string
  end

  def show
    mentar = Mentar.includes(comments: :sender).find(params[:id])

    options = {}
    options[:include] = [:comments]
    json_string = MentarSerializer.new(mentar, options).serialized_json
    render json: json_string
  end
end
