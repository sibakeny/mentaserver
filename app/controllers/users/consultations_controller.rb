# frozen_string_literal: true

class Users::ConsultationsController < ApplicationController
  def index
    user = User.find_by(token: params[:token])
    consultations = Consultation.includes(:user).where(user_id: user.id).order(status: :asc).order(created_at: :desc)
    json_string = ConsultationSerializer.new(consultations).serialized_json
    render json: json_string
  end
end
