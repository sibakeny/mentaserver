# frozen_string_literal: true

class Users::WithIdController < ApplicationController
  def show
    user = User.find(params[:id])

    json_string = UserSerializer.new(user).serialized_json
    render json: json_string
  end
end
