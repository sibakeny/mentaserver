# frozen_string_literal: true

class Users::MentarsController < ApplicationController
  def show
    user = User.find_by(token: params[:user_id])
    mentar = Mentar.includes(comments: :sender).find_by(user_id: user.id)
    options = {}
    options[:include] = [:comments]
    json_string = MentarSerializer.new(mentar, options).serialized_json
    render json: json_string
  end

  def update
    user = User.find_by(token: params[:token])
    mentar = Mentar.find_or_initialize_by(user_id: user.id)
    mentar.update(description: params[:description], category: params[:category], sex: params[:sex])
    mentar.save
  end
end
