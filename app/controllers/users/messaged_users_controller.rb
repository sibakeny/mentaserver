# frozen_string_literal: true

class Users::MessagedUsersController < ApplicationController
  def index
    user = User.find_by(token: params[:user_id])
    senders_ids = Comment.where(reciever_id: user.id, target_type: nil).map(&:sender_id).to_a.uniq
    recievers_ids = Comment.where(sender_id: user.id, target_type: nil).map(&:reciever_id).to_a.uniq
    user_ids = (senders_ids + recievers_ids).uniq
    messaged_users = User.includes(:mentar, { image_attachment: :blob }).find(user_ids)

    json_string = UserSerializer.new(messaged_users, params: { currentUserId: user.id, showNotification: true }).serialized_json
    render json: json_string
  end
end
