# frozen_string_literal: true

class Notifications::AllController < ApplicationController
  def destroy
    notifications = Notification.all

    notifications = notifications.where(sender_id: params[:sender_id]) if params[:sender_id].present?
    notifications = notifications.where(category: params[:category]) if params[:category].present?
    notifications = notifications.where(opponent_id: params[:opponent_id]) if params[:opponent_id].present?
    notifications.each(&:destroy!)

    render json: { status: "success" }
  end
end
