# frozen_string_literal: true

class User < ApplicationRecord
  has_secure_token

  has_many :direct_message_notifications
  has_one_attached :image
  has_one :mentar
  has_many :comments, class_name: "Comment", foreign_key: "sender_id"
  has_many :user_logs

  validates :email, presence: true
  validates :name, presence: true

  enum status: { active: 0, stop: 1 }

  def create_login_log
    UserLog.create(user_id: self.id, action_type: "login")
  end

  def last_login_date
    user_logs&.order(created_at: :desc)&.first&.strftime("%Y%m%d")  
  end

  def freeze_account
    stop!
  end

  def restart_account
    active!
  end
end
