# frozen_string_literal: true

class Mentar < ApplicationRecord
  belongs_to :user
  has_many :comments, as: :target
  has_many :notifications, as: :opponent

  def self.flex_sort(column, order)
    order("#{column} #{order}") if column.present?
  end

  def self.filter(params)
    if params[:keyword] != ""
      @mentars = @mentars.joins(:user).where("mentars.description LIKE ? OR users.name LIKE ?", "%" + params[:keyword] + "%", "%" + params[:keyword] + "%")
    end
    if params[:category].present?
      @mentars = @mentars.where(category: params[:category])
    end
    @mentars = @mentars.where(sex: params[:sex]) if params[:sex].present?
    if params[:orderCreatedAt] == "true"
      @mentars = @mentars.order(created_at: :desc)
    end
  end
end
