# frozen_string_literal: true

class Consultation < ApplicationRecord
  belongs_to :user
  has_many :comments, as: :target
  has_many :notifications, as: :opponent

  enum status: { open: 0, pending: 1, close: 2, resolved: 3 }

  def self.flex_sort(column, _order)
    if column == "comments"
    end
  end

  def self.filter(params)
    if params[:keyword] != ""
      @consultations = @consultations.
                         where("title LIKE ?", "%" + params[:keyword] + "%").
                         or(consultation.where(description: params[:keyword]))
    end
    if params[:category].present?
      @consultations = @consultations.where(category: params[:category])
    end

    if params[:sex].present? && params[:sex] != "0"
      @consultations = @consultations.where(sex: params[:sex])
    end

    return @consultations
  end
end
