# frozen_string_literal: true

class Comment < ApplicationRecord
  belongs_to :target, polymorphic: true, optional: true
  belongs_to :sender, class_name: "User", foreign_key: "sender_id"
  belongs_to :reciever, class_name: "User", foreign_key: "reciever_id", optional: true
  belongs_to :user, optional: true

  def self.find_by_params(target_id: nil, target_type: nil, sender_id: nil, reciever_id: nil)
    # target_id, target_typeのパラメータがある場合はConsultationのcommentかMentarのcomment
    comments = if target_id.present? && target_type.present?
                 Comment.where(
                   target_id: target_id,
                   target_type: target_type,
                 )
               else
                 # target_id, target_typeが無い場合はdirectMessageのもの
                 Comment.where(
                   sender_id: sender_id,
                   reciever_id: reciever_id,
                   target_type: nil,
                 )
               end

    comments
  end
end
