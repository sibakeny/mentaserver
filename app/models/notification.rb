# frozen_string_literal: true

class Notification < ApplicationRecord
  enum category: { directMessage: 0, Mentar: 1, Consultation: 2 }

  belongs_to :opponent, polymorphic: true, optional: true
  belongs_to :user
  belongs_to :sender, class_name: "User", foreign_key: "sender_id"
end
