# frozen_string_literal: true

class DirectMessageNotification < ApplicationRecord
  belongs_to :user
  belongs_to :sender, class_name: "User", foreign_key: "sender_id"

  default_scope { where(active: true) }
end
