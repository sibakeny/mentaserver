class UserLog < ApplicationRecord
    belongs_to :user

    enum action_type: { login: 0 }
end
