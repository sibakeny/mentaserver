# frozen_string_literal: true

class DropDirectMessageNotification < ActiveRecord::Migration[6.0]
  def change
    drop_table :direct_message_notifications
  end
end
