# frozen_string_literal: true

class AddTargetToComments < ActiveRecord::Migration[6.0]
  def change
    add_reference :comments, :target, polymorphic: true, index: true
  end
end
