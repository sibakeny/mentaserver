# frozen_string_literal: true

class AddCategorySexToMentar < ActiveRecord::Migration[6.0]
  def change
    add_column :mentars, :category, :string
    add_column :mentars, :sex, :integer
  end
end
