# frozen_string_literal: true

class CreateConsultations < ActiveRecord::Migration[6.0]
  def change
    create_table :consultations do |t|
      t.string :category
      t.integer :sex
      t.string :title
      t.text :description
      t.integer :user_id

      t.timestamps
    end
  end
end
