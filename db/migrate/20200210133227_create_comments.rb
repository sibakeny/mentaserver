# frozen_string_literal: true

class CreateComments < ActiveRecord::Migration[6.0]
  def up
    create_table :comments do |t|
      t.references :sender, foreign_key: { to_table: :users }
      t.references :reciever, foreign_key: { to_table: :users }
      t.string :body

      t.timestamps
    end

    drop_table :direct_message_comments
    drop_table :mentar_evaluation_comments
    drop_table :consultation_comments
  end

  def down
    drop_table :comments

    create_table :direct_message_comments do |t|
      t.string :body
      t.integer :sender_id
      t.integer :reciever_id

      t.timestamps
    end

    create_table :mentar_evaluation_comments do |t|
      t.integer :mentar_id
      t.string :body
      t.integer :user_id

      t.timestamps
    end

    create_table :consultation_comments do |t|
      t.string :body
      t.integer :consultation_id

      t.timestamps
    end
  end
end
