# frozen_string_literal: true

class AddActiveToDirectMessageNotification < ActiveRecord::Migration[6.0]
  def change
    add_column :direct_message_notifications, :active, :boolean, default: true
  end
end
