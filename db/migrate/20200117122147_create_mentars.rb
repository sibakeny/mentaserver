# frozen_string_literal: true

class CreateMentars < ActiveRecord::Migration[6.0]
  def change
    create_table :mentars do |t|
      t.text :description

      t.timestamps
    end
  end
end
