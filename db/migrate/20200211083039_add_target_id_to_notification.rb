# frozen_string_literal: true

class AddTargetIdToNotification < ActiveRecord::Migration[6.0]
  def change
    add_reference :notifications, :opponent, polymorphic: true, index: true
  end
end
