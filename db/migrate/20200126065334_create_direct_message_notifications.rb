# frozen_string_literal: true

class CreateDirectMessageNotifications < ActiveRecord::Migration[6.0]
  def change
    create_table :direct_message_notifications do |t|
      t.integer :user_id
      t.integer :sender_id

      t.timestamps
    end
  end
end
