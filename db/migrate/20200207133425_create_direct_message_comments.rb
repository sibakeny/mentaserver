# frozen_string_literal: true

class CreateDirectMessageComments < ActiveRecord::Migration[6.0]
  def change
    create_table :direct_message_comments do |t|
      t.string :body
      t.integer :sender_id
      t.integer :reciever_id

      t.timestamps
    end
  end
end
