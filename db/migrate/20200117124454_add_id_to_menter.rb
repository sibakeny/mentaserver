# frozen_string_literal: true

class AddIdToMenter < ActiveRecord::Migration[6.0]
  def change
    add_reference :mentars, :user
  end
end
