# frozen_string_literal: true

class CreateNotifications < ActiveRecord::Migration[6.0]
  def change
    create_table :notifications do |t|
      t.integer :category
      t.integer :user_id
      t.integer :sender_id

      t.timestamps
    end
  end
end
