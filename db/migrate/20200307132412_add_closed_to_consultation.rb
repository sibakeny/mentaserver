# frozen_string_literal: true

class AddClosedToConsultation < ActiveRecord::Migration[6.0]
  def change
    add_column :consultations, :status, :integer, default: 0
  end
end
