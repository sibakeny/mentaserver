class CreateUserLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :user_logs do |t|
      t.references :user
      t.integer :action_type, null: false

      t.timestamps
    end
  end
end
