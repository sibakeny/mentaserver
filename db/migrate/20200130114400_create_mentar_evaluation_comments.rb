# frozen_string_literal: true

class CreateMentarEvaluationComments < ActiveRecord::Migration[6.0]
  def change
    create_table :mentar_evaluation_comments do |t|
      t.integer :mentar_id
      t.string :body
      t.integer :user_id

      t.timestamps
    end
  end
end
