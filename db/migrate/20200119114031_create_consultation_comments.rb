# frozen_string_literal: true

class CreateConsultationComments < ActiveRecord::Migration[6.0]
  def change
    create_table :consultation_comments do |t|
      t.string :body
      t.integer :consultation_id

      t.timestamps
    end
  end
end
