class AddStatusToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :status, :integer, nulll: false, default: 0
  end
end
